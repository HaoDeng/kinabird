# coding: utf-8
require 'nokogiri'
require 'open-uri'
require 'date'
require 'fastimage'

namespace :parse do

  desc "get magazines"
  task :get_magazines => :environment do     
    page = Nokogiri::HTML open("http://www.tilbudsugen.dk/")  
    page.css('.formShow').each do |section|
      category = section.css('td strong').children.text

      section.css('tr .super_list').each do |shop|
        parsing_magazine_shop(category, shop)
      end     
    end
   
  end # end of task

  private
  def parsing_magazine_shop(category, shop)
    if shop.css('img').empty?
      shop_name = shop.css('.super_image_placeholder').children.text
    else
      logo_value = shop.css('img').first.attributes['style'].value
      logo_url = logo_value[logo_value.index("http://")...logo_value.index(".jpeg")+5] if logo_value
      
      name_value = shop.css('img').first.attributes['alt'].value
      shop_name = name_value[0...name_value.index('tilbudsavis')].strip
    end
    
    
    shop.css('a').each do |shop_link|
      link_attr = shop_link.attributes['onclick']
      link_value = link_attr.value if link_attr
      shop_url = "http://www.tilbudsugen.dk/" + link_value[link_value.index("show_paper_new.php")...link_value.index("','popup'")] if link_value
      
      if shop_url
        from_to_dates = shop_link.css('font').children.text.split('-')
        from_date = DateTime.strptime(from_to_dates[0].strip, '%d/%m')
        to_date = DateTime.strptime(from_to_dates[1].strip, '%d/%m')

        create_magazine(category, shop_name, logo_url, shop_url, from_date, to_date)
      end       
    end
  end

  def create_magazine(category, shop_name, logo_url, shop_url, from_date, to_date)
    m = Magazine.new
    m.super_market = shop_name
    m.magazine_type = category
    m.from_date = from_date
    m.to_date = to_date
    m.logo_url = logo_url

    exist = Magazine.where(:super_market => m.super_market, 
                           :from_date => m.from_date,
                           :to_date => m.to_date).first
    
    unless exist
      p "loading Magazine: #{m.super_market}, #{m.from_date} - #{m.to_date}"
      create_magazine_pages(m, shop_url)
      m.save
    end
  
    sleep(1)
  end


  def create_magazine_pages(magazine, shop_url)  
    page = Nokogiri::HTML open(shop_url)
    page_img_url = page.css('#show_paper_left a img').first.attributes['data-magnifysrc'].value
    
    pagination_dom = page.css('#show_paper_navigator table tr td a').children
    max_page_nr = pagination_dom.max_by { |p| p.text.to_i}.text.to_i

    (1..max_page_nr).each do |page_number|
      create_magazine_page(magazine, page_number, page_img_url)
    end
  end

  def create_magazine_page(magazine, page_number, page_img_url)
    page = magazine.magazine_pages.build
    page_img_url = page_img_url.gsub("0.jpg", "#{page_number-1}.jpg")
    page.image_url = page_img_url
    page.page_number = page_number

    exist = MagazinePage.where(:image_url => page.image_url).first
    unless exist
      page.save
      p "creating #{magazine.super_market}, page: #{page_number}"
    end
  end

end