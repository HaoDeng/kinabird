require 'feedzirra'
require 'htmlentities'
require 'nokogiri'
require 'open-uri'

namespace :parse do

  desc "get events from eventbrite"
  task :get_events => :environment do

    (0..eventbrites_uris.size-1).each do |i|
      p "Loading events in city #{eventbrites_uris[i][:location]}"
      feed = Feedzirra::Feed.fetch_and_parse(eventbrites_uris[i][:url])

      feed.entries.each do |entry|
        create_event_brite(entry, eventbrites_uris[i][:location])
      end
      sleep(1)
    end
  end # end of task

  private
  def eventbrites_uris
    [
      { :location => 'Copenhagen',  :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Copenhagen&is_miles=True&city=Copenhagen&slat=55.68&slng=12.57&radius=60.0&lat=55.68&lng=12.57'},
      # Hellerup, Copenhagen
      { :location => 'Copenhagen',  :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Copenhagen&is_miles=True&city=Hellerup&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833'},
      # Lyngby, Copenhagen
      { :location => 'Copenhagen',  :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Copenhagen&is_miles=True&city=Lyngby&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833'},
      # Allerød, Copenhagen
      { :location => 'Copenhagen',  :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aller%C3%B8d&is_miles=True&city=Aller%C3%B8d&slat=55.68&slng=12.57&radius=60.0&lat=55.68&lng=12.57'},
      # Frederikssund, Copenhagen
      { :location => 'Copenhagen',  :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Frederikssund&is_miles=True&city=Frederikssund&slat=55.68&slng=12.57&radius=60.0&lat=55.68&lng=12.57'},
      # København, Copenhagen
      { :location => 'Copenhagen',  :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=K%C3%B8benhavn&is_miles=True&city=K%C3%B8benhavn&slat=55.68&slng=12.57&radius=60.0&lat=55.68&lng=12.57'},     
      # Vordingborg
      { :location => 'Vordingborg', :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Copenhagen&is_miles=True&city=Vordingborg&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833'},
      # Roskilde
      { :location => 'Roskilde',    :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Copenhagen&is_miles=True&city=Roskilde+County&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833'},
      # Aarhus
      { :location => 'Aarhus',      :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Aarhus&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Kolding
      { :location => 'Kolding',     :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Kolding&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Aalborg
      { :location => 'Aalborg',     :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Aalborg&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Horsens
      { :location => 'Horsens',     :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Horsens&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Randers
      { :location => 'Randers',     :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Randers&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Brabrand
      { :location => 'Brabrand',    :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Brabrand&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Skive
      { :location => 'Skive',       :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Skive&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Skjern
      { :location => 'Skjern',      :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Skjern&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Vejle
      { :location => 'Vejle',       :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Aarhus&is_miles=True&city=Vejle&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'},
      # Korsor
      { :location => 'Korsor',      :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Korsor&is_miles=True&city=Korsor&slat=56.16&slng=10.2&radius=60.0&lat=56.16&lng=10.2'},     
      # Odense
      { :location => 'Odense',      :url => 'http://www.eventbrite.com/directoryxml/?sort=date&loc=Odense&is_miles=True&city=Odense&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785'}
    ]
  end

  def create_event_brite (entry, city)
    event = Event.new
    event.name = entry.title

    time_location = entry.summary.split("\n")

    date_time = time_location.select { |p| p.include?('Begins:')}
    event.event_date_str = date_time[0].gsub!("Begins:","").strip unless date_time.empty?
    event.event_date = DateTime.parse(event.event_date_str) if event.event_date_str

    location = time_location.select { |p| p.include?('Where:')}
    event.location = location[0].gsub!("Where:","").strip unless location.empty?

    event.city = city
    event.event_url = entry.id
    event.event_type = entry.categories.join(",")
    event.provider = 'EventBrite'

    exist = Event.where(:name => event.name, 
                        :event_date => event.event_date,
                        :location => event.location).first
    unless exist
      event.logo_url = get_eventbrite_logo_url(event.event_url)
      event.save 
      p "loading event: #{event.name} "
    end
  end

  private
  def get_eventbrite_logo_url(event_url)
    p "loading logo_url from #{event_url}"
    begin
      page = Nokogiri::HTML open(event_url)  
      img_dom = page.css('#subheader_table .event_title_image img')
      img_dom.first.attributes['src'].value unless img_dom.empty?
    rescue Exception => ignore
    end
  end
end