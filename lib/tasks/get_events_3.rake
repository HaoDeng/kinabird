# coding: utf-8
require 'nokogiri'
require 'open-uri'
require 'date'

namespace :parse do

  desc "get events from billetlugen"
  task :get_events_3 => :environment do

    (0..billetlugen_urls.size-1).each do |i|
      p "loading events in city #{billetlugen_urls[i][:location]}, type #{billetlugen_urls[i][:type]}"

      (1..100).each do |p|
        p "loading page #{p}"
        page = Nokogiri::HTML open(billetlugen_urls[i][:url] << p.to_s)  
        
        
        if page.css('.event-list table tbody tr').empty?
          p 'Loading complete!'
          sleep(10)
          break
        end

        page.css('.event-list table tbody tr').each {
          |dom| create_event_billetlugen(dom, 
                                         billetlugen_urls[i][:location], 
                                         billetlugen_urls[i][:type])
        }

        sleep(10)
      end # end of each page
    end

    
  end # end of task

  private 
  def billetlugen_urls
    [
      { :location => 'Copenhagen',     :type => 'Music',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=2&t=0&l=1&v=0&p='},
      { :location => 'Sjælland + øer', :type => 'Music',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=2&t=0&l=2&v=0&p='},
      { :location => 'Fyn og øerne',   :type => 'Music',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=2&t=0&l=3&v=0&p='},
      { :location => 'Jylland',        :type => 'Music',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=2&t=0&l=4&v=0&p='},
      { :location => 'Bornholm',       :type => 'Music',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=2&t=0&l=5&v=0&p='},
      { :location => 'Copenhagen',     :type => 'Sport',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=1&t=0&l=1&v=0&p='},
      { :location => 'Sjælland + øer', :type => 'Sport',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=1&t=0&l=2&v=0&p='},
      { :location => 'Fyn og øerne',   :type => 'Sport',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=1&t=0&l=3&v=0&p='},
      { :location => 'Jylland',        :type => 'Sport',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=1&t=0&l=4&v=0&p='},
      { :location => 'Bornholm',       :type => 'Sport',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=1&t=0&l=5&v=0&p='},
      { :location => 'Copenhagen',     :type => 'Teater', :url => 'http://www.billetlugen.dk/forside/list/dato/?e=3&t=0&l=1&v=0&p='},
      { :location => 'Sjælland + øer', :type => 'Teater', :url => 'http://www.billetlugen.dk/forside/list/dato/?e=3&t=0&l=2&v=0&p='},
      { :location => 'Fyn og øerne',   :type => 'Teater', :url => 'http://www.billetlugen.dk/forside/list/dato/?e=3&t=0&l=3&v=0&p='},
      { :location => 'Jylland',        :type => 'Teater', :url => 'http://www.billetlugen.dk/forside/list/dato/?e=3&t=0&l=4&v=0&p='},
      { :location => 'Bornholm',       :type => 'Teater', :url => 'http://www.billetlugen.dk/forside/list/dato/?e=3&t=0&l=5&v=0&p='},
      { :location => 'Copenhagen',     :type => 'Other',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=4&t=0&l=1&v=0&p='},
      { :location => 'Sjælland + øer', :type => 'Other',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=4&t=0&l=2&v=0&p='},
      { :location => 'Fyn og øerne',   :type => 'Other',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=4&t=0&l=3&v=0&p='},
      { :location => 'Jylland',        :type => 'Other',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=4&t=0&l=4&v=0&p='},
      { :location => 'Bornholm',       :type => 'Other',  :url => 'http://www.billetlugen.dk/forside/list/dato/?e=4&t=0&l=5&v=0&p='}
      
    ]
  end

  def create_event_billetlugen (dom, city, type)
    event = Event.new
    event.event_url = 'http://www.billetlugen.dk' << dom.css('td.event a').first.attributes['href'].value

    event.name = dom.css('td.event a strong').children.text

    datetime = dom.css('td.date span').children.text << ", " << dom.css('td.date strong').children.text
    event.event_date = DateTime.strptime(datetime, '%d/%m/%y, %H:%M') if datetime
    event.event_date_str = event.event_date.strftime('%B %d, %Y, %H:%M') if event.event_date
    event.location = dom.css('td.location a strong').children.text

    event.city = city
    event.event_type = type
    event.provider = 'Billetlugen'

    exist = Event.where(:name => event.name, 
                        :event_date => event.event_date,
                        :location => event.location).first
    unless exist
      page = Nokogiri::HTML open(event.event_url)
      img_dom = page.css('#event-image img')   
      event.logo_url = img_dom.first.attributes['src'].value unless img_dom.empty?

      event.save 
      p "loading event: #{event.name} "
      sleep(10)
    end
  end

end