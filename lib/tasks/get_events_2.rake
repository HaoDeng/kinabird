require 'feedzirra'
require 'htmlentities'

namespace :parse do

  desc "get events from lanyrd"
  task :get_events_2 => :environment do

    (0..lanyrd_uris.size-1).each do |i|
      feed = Feedzirra::Feed.fetch_and_parse(lanyrd_uris[i][:url])

      feed.entries.each do |entry|
        create_event_lanyrd(entry, lanyrd_uris[i][:location])
      end
      sleep(1)
    end
  end # end of task

  private
  def lanyrd_uris
    [
      { :location => 'Copenhagen', :url => 'http://lanyrd.com/places/copenhagen/feed/'},
      { :location => 'Horsens',    :url => 'http://lanyrd.com/places/554309/feed/'},
      { :location => 'Arhus',      :url => 'http://lanyrd.com/places/28583790/feed/'},
      { :location => 'Odense',     :url => 'http://lanyrd.com/places/556053/feed/'},
      { :location => 'Aalborg',    :url => 'http://lanyrd.com/places/12582734/feed/'},
      { :location => 'Korsor',     :url => 'http://lanyrd.com/places/554967/feed/'},
      { :location => 'Roskilde',   :url => 'http://lanyrd.com/places/12582781/feed/'},
      { :location => 'Billund',    :url => 'http://lanyrd.com/places/12582750/feed/'},
      { :location => 'Vejle',      :url => 'http://lanyrd.com/places/12582833/feed/'},
      { :location => 'Kolding',    :url => 'http://lanyrd.com/places/12582844/feed/'},
      { :location => 'Hjorring',   :url => 'http://lanyrd.com/places/12582733/feed/'}
    ]
  end

  def create_event_lanyrd (entry, city)
    event = Event.new
    event.name = entry.title
    event.event_date = DateTime.parse(entry.title)
    event.location = city
    event.city = city
    event.event_url = entry.url
    event.event_type = 'Conference'
    event.provider = 'Lanyrd'
    event.event_date_str = event.event_date.strftime('%B %d, %Y, %H:%M') if event.event_date

    exist = Event.where(:name => event.name, 
                        :event_date => event.event_date,
                        :location => city).first
    unless exist
      event.save 
      p "loading event: #{event.name} "
    end
  end
end