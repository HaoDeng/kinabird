require 'nokogiri'
require 'open-uri'
require 'date'

namespace :parse do

  desc "get events from eventbrite"
  task :get_events => :environment do

    (0..location_urls.size-1).each do |i|
      p "loading events in city #{location_urls[i][:location]}"

      (1..100).each do |p|
        p "loading page #{p}"
        page = Nokogiri::HTML open(get_event_brite_uri(location_urls[i][:url], p))  
        
        if page.css('tr.event_listing_item').empty?
          p 'Loading complete!'
          break
        end

        k = 0 # retry max 20 times
        until validate_event_brite?(page) && k< 20
          p "reloading page #{p}"
          sleep(5)
          page = Nokogiri::HTML open(get_event_brite_uri(location_urls[i][:url], p))
          k +=1
        end

        page.css('tr.event_listing_item').each {
          |dom| create_event_brite(dom, location_urls[i][:location])
        }

        sleep(1)
      end # end of each page
    end

  end # end of task

  private
  def location_urls
    [
      # Copenhagen
      { :location => 'Copenhagen',  :url => 'directory?sort=date&loc=Copenhagen&is_miles=True&city=Copenhagen&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833&page='},
      # Hellerup, Copenhagen
      { :location => 'Copenhagen',  :url => 'directory?sort=date&loc=Copenhagen&is_miles=True&city=Hellerup&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833&page='},
      # Lyngby, Copenhagen
      { :location => 'Copenhagen',  :url => 'directory?sort=date&loc=Copenhagen&is_miles=True&city=Lyngby&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833&page='},
      # Vordingborg
      { :location => 'Vordingborg', :url => 'directory?sort=date&loc=Copenhagen&is_miles=True&city=Vordingborg&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833&page='},
      # Roskilde
      { :location => 'Roskilde',    :url => 'directory?sort=date&loc=Copenhagen&is_miles=True&city=Roskilde+County&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833&page='},
      # Helsingborg
      { :location => 'Helsingborg', :url => 'directory?sort=date&loc=Copenhagen&is_miles=True&city=Helsingborg&slat=55.6667&slng=12.5833&radius=60.0&lat=55.6667&lng=12.5833&page='},
      # Aarhus
      { :location => 'Aarhus',      :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Aarhus&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Kolding
      { :location => 'Kolding',     :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Kolding&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Aalborg
      { :location => 'Aalborg',     :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Aalborg&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Horsens
      { :location => 'Horsens',     :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Horsens&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Randers
      { :location => 'Randers',     :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Randers&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Brabrand
      { :location => 'Brabrand',    :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Brabrand&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Skive
      { :location => 'Skive',       :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Skive&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Skjern
      { :location => 'Skjern',      :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Skjern&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Vejle
      { :location => 'Vejle',       :url => 'directory?sort=date&loc=Aarhus&is_miles=True&city=Vejle&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='},
      # Odense
      { :location => 'Odense',      :url => 'directory?sort=date&loc=Odense&is_miles=True&city=Odense&slat=56.26392&slng=9.501785&radius=60.0&lat=56.26392&lng=9.501785&page='}
    ]
  end

  def get_event_brite_uri(location_url, page = 1)
    uri = "http://www.eventbrite.com/"
    uri << location_url
    uri << page.to_s 
  end

  def validate_event_brite?(page)
    when_where_dom = page.css('tr.event_listing_item td.info tbody').children.children.children.to_a
    !when_where_dom.nil? && !when_where_dom.empty?
  end

  def create_event_brite(dom, city)
    logo_url = dom.css('.logo a img').first.attributes['src'].value
    name = dom.css('td.info h3 a').children.first.text
    name = name.strip if name

    event_url = dom.css('td.info h3 a').first.attributes['href'].value

    event_type = dom.css('td.info p.channels').children.text
    event_type = event_type.strip if event_type

    when_where_dom = dom.css('td.info tbody').children.children.children.to_a    
    if when_where_dom[0].text == "When:"
      event_date_str = when_where_dom[1].text if when_where_dom[1]
      event_location = when_where_dom[3].text if when_where_dom[3]
    end

    # ignore started events, ignore null date events
    return if event_date_str.nil? || event_date_str.include?("Started")

    event = Event.new
    event.name = name
    event.logo_url = logo_url
    event.event_url = event_url
    event.event_type = (event_type.nil? || event_type.empty?) ? 'Conference' : event_type
    event.event_date_str = event_date_str
    event.event_date = to_date(event_date_str)
    event.location = event_location
    event.city = city
    event.provider = 'EventBrite'

    exist = Event.where(:name => event.name, 
                        :event_date_str => event_date_str,
                        :location => event_location).first
    unless exist
      event.save 
      p "loading event: #{event.name} "
    end
  end

  def to_date(str)
    return nil if str.nil?

    date_str = str
    date_str = str[0..str.index('(')-1].strip if str.include?('(')

    DateTime.parse(date_str)
  end

end 