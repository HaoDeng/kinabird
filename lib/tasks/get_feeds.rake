#encoding: utf-8
require 'feedzirra'
require 'htmlentities'

namespace :parse do

  desc "get news from google"
  task :get_feeds => :environment do

    (0..news_uris.size-1).each do |i|
      feed = Feedzirra::Feed.fetch_and_parse(news_uris[i][:link])
      feed.entries.each do |entry|
        process_news(entry, news_uris[i][:provider], news_uris[i][:language])
      end
      sleep(1)
    end

    # get the content of unprocessed news
    get_news_content
  end # end of task

  private
  def news_uris
    [
      # 丹麦 - CH
      {:provider =>'baidu', :language =>'CH', :link =>"http://news.baidu.com/ns?word=title%3A%B5%A4%C2%F3&tn=newsrss&sr=0&cl=2&rn=20&ct=0"},
      # 哥本哈根 - CH
      {:provider =>'baidu', :language =>'CH', :link =>"http://news.baidu.com/ns?word=title%3A%B8%E7%B1%BE%B9%FE%B8%F9&tn=newsrss&sr=0&cl=2&rn=20&ct=0"},
      # 丹麦市场 - CH
      {:provider =>'baidu', :language =>'CH', :link =>"http://news.baidu.com/ns?word=title%3A%B5%A4%C2%F3%CA%D0%B3%A1&tn=newsrss&sr=0&cl=2&rn=20&ct=0"},
      # 丹麦 - CH
      {:provider =>'google', :language =>'CH', :link =>"http://news.google.com/news?hl=en&q=%E4%B8%B9%E9%BA%A6&bav=on.2,or.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 哥本哈根 - CH
      {:provider =>'google', :language =>'CH', :link =>"http://news.google.com/news?q=%E5%93%A5%E6%9C%AC%E5%93%88%E6%A0%B9&hl=en&bav=on.2,or.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # Danish - EN
      {:provider =>'google', :language =>'EN', :link =>"https://news.google.com/news/feeds?hl=en&q=danish&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # Copenhagen - EN
      {:provider =>'google', :language =>'EN', :link =>"https://news.google.com/news/feeds?hl=en&q=copenhagen&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # Denmark - EN
      {:provider =>'google', :language =>'EN', :link =>"https://news.google.com/news/feeds?hl=en&q=denmark&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # CPH post - EN
      {:provider =>'google', :language =>'EN', :link =>"https://news.google.com/news/feeds?q=source:cphpost.dk&num=30&hl=en&gl=us&as_qdr=a&authuser=0&bav=on.2,or.r_cp.&bvm=bv.44442042,d.Yms&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # Denmark - CH (Traditional)
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?q=%E4%B8%B9%E9%BA%A5&hl=en&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=600&um=1&ie=UTF-8&output=rss"},
      # 中丹关系 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?hl=en&gl=us&q=%22%E4%B8%AD%E4%B8%B9%E5%85%B3%E7%B3%BB%22&bav=on.2,or.r_cp.r_qf.&bvm=bv.43287494,d.Yms&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 中丹贸易 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?hl=en&gl=us&q=%22%E4%B8%AD%E4%B8%B9%E8%B4%B8%E6%98%93%22&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 丹麦首都 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?q=%E4%B8%B9%E9%BA%A6%E9%A6%96%E9%83%BD&hl=en&gl=us&tbas=0&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 丹麦政府 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?hl=en&gl=us&q=%22%E4%B8%B9%E9%BA%A6%E6%94%BF%E5%BA%9C%22&psj=1&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 哥本哈根市 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?hl=en&gl=us&q=%22%E5%93%A5%E6%9C%AC%E5%93%88%E6%A0%B9%E5%B8%82%22&psj=1&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 丹麦公司 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?hl=en&gl=us&q=%22%E4%B8%B9%E9%BA%A6%E5%85%AC%E5%8F%B8%22&psj=1&bav=on.2,or.r_cp.r_qf.&bvm=bv.44442042,d.Yms&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 丹麦市场 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?hl=en&gl=us&q=%22%E4%B8%B9%E9%BA%A6%E5%B8%82%E5%9C%BA%22&psj=1&bav=on.2,or.r_cp.r_qf.&biw=1366&bih=643&um=1&ie=UTF-8&output=rss"},
      # 大纪元 - CH
      {:provider =>'google', :language =>'CH', :link =>"https://news.google.com/news/feeds?gl=us&hl=en&as_occt=any&as_qdr=a&as_nsrc=大纪元&authuser=0&q=丹麦+source:大纪元&um=1&ie=UTF-8&output=rss"}   
    ]
  end

  def news_source_blacklist
    [
      '52pk游戏网',
      '亿房网',
      '商都娱乐',
      '经营网',
      '中国江西网',
      '资阳网',
      '中国建筑学会',
      '杭州网',
      '21ic中国电子网',
      '中广互联',
      '木蚂蚁网络',
      '中国投影网',
      '同花顺金融服务网'
    ]
  end

  def news_title_blacklist
    [
      '丹麦足球',
      '丹麦对',
      '对丹麦',
      'vs丹麦',
      '丹麦vs'
    ]
  end

  def process_news(entry, provider, language)
    article = Article.new
    article.language = language
    article.guid = entry.entry_id
    
    html_coder = HTMLEntities.new

    # parse title and source
    if 'google' == provider
      title = entry.title[0..entry.title.rindex('-')-1].strip
      article.source = entry.title[entry.title.rindex('-')+1..entry.title.size].strip
      url = entry.url
      article.link = url[url.rindex("url=http")+4, url.size]
      article.published_at = entry.published   

      imgNode = entry.summary.split(/<img src=\"\/\//)[1]
      unless imgNode.nil?
        imgurl = imgNode[0..imgNode.index('"')-1] 
        imgurl = imgurl.prepend("http://") unless imgurl.start_with?("http://")
        article.imgurl = imgurl
      end  
    else
      title = entry.title
      article.source = entry.author
      article.link = entry.url
      if entry.author.nil?
        return
      end

      if news_source_blacklist.include?(entry.author) || entry.author.include?('楼盘网')
        return
      end

      news_title_blacklist.each do |t|
        return if entry.title.include?(t)
      end
      article.published_at = entry.published - 6.hours
    end

    summary = ActionController::Base.helpers.strip_tags(entry.summary)
    summary = summary.gsub(article.title, "") if article.title
    summary = summary.gsub(article.source,"") if article.source
    summary = html_coder.decode(summary)
    article.summary = summary

    title.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
    title.encode!('UTF-8', 'UTF-16')
    title = html_coder.decode(title)
    article.title = title  

    exist = Article.where(:title => article.title).first
    unless exist
      article.save 
      p "loading article: #{article.title}, #{article.source}, #{provider} "
    end
  end
end