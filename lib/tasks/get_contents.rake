#encoding: UTF-8
require 'htmlentities'
require 'nokogiri'
require 'open-uri'
require 'watir-webdriver'
require 'headless'


namespace :parse do

  desc "parsing news contents"
  task :get_contents_test => :environment do
    page = get_content_page("腾讯体育", "http://sports.qq.com/a/20130422/000007.htm")

    dom_hash = try_get_content_dom(page)

    contents = ActionController::Base.helpers.strip_tags(
        dom_hash[:dom].to_html.gsub("<br><br>","<br>")
                              .gsub("<br>","###NEW_LINE###"))

    p contents.gsub("###NEW_LINE###", "<br><br>")
    p dom_hash[:img_dom].first.attributes['src'].value
    p dom_hash[:img_dom].last.attributes['src'].value
  end

  task :get_contents => :environment do
    get_news_content
  end # end of task

  def get_news_content
    articles = Article.where("content is null and status = 0").order(:published_at).limit(100)

    articles.each do |article|
      begin  
        p "Fetching the content of article: #{article.title}, source: #{article.source}, id: #{article.id}"
        page = get_content_page(article.source, article.link)

        dom_hash = try_get_content_dom(page)

        contents = ActionController::Base.helpers.strip_tags(
            dom_hash[:dom].to_html.gsub("<br><br>","<br>")
                                  .gsub("<br>","###NEW_LINE###"))

        article.content = contents.gsub("###NEW_LINE###", "<br><br>")

        unless dom_hash[:img_dom].empty?
            article.content_img_url = get_2_content_imgs(dom_hash[:img_dom], article.link)                                           
        end
        article.save

      rescue Exception => e
        p e.message  
        p e.backtrace.inspect
        if content_error_code.any? { |error| e.message.include? error }
            article.status = 9
            article.save
        elsif e.message.include?("Timeout") || e.message.include?("timed out")
            article.status = 10
            article.save
        else
            article.status = 11
            article.save
        end
      end  
      
      sleep(1)
    end
  end


  private
  def get_content_page(source, uri)
    if ajax_loading_source?(source)
      begin
        headless = Headless.new
        headless.start
        client = Selenium::WebDriver::Remote::Http::Default.new
        client.timeout = 120 
        browser = Watir::Browser.new :firefox, :http_client => client
        p "loading by headless webdriver, source: #{source}"
        browser.goto(uri)
          
        page = Nokogiri::HTML(browser.html)       
      ensure
        browser.close if browser
        headless.destroy if headless
      end
    else
      page = Nokogiri::HTML open(uri)
    end

    page
  end

  def ajax_loading_source?(source)
    ["腾讯网", "游侠网", "香港新浪網", "香港文匯報", "DesMoinesRegister.com",
     "新民晚报", "凤凰网 (博客)", "Green Bay Press Gazette",
     "The Province", "Yahoo! Eurosport UK", 
     "腾讯体育","腾讯财经"].include?(source)
  end

  def content_error_code
    ["300", "302", "400", "401", "402", "403", "404", "500", "501","502",
     "getaddrinfo"
    ]
  end

  def try_get_content_dom(page)  
    content_dom = nil
    content_keywords.each do |key|
      content_dom = page.css(key)
      unless content_dom.empty?
        p "use #{key} to fetch content!"
        break
      end
    end

    # default
    content_dom = page if content_dom.nil? || content_dom.empty?

    # try to get the image in page
    img_dom = nil
    img_keywords.each do |key|
      img_dom = page.css(key)
      unless img_dom.empty?
        p "use #{key} to fetch image!"
        break
      end
    end
    img_dom = content_dom.css('img') if img_dom.nil? || img_dom.empty?
 
    # try to fetch paragragh
    para_dom = content_dom.css('p')

    para_dom.each do |d|
      (d.content = d.content.strip + "###NEW_LINE###") unless d.content.strip.empty?
    end

    content_dom = para_dom if !para_dom.nil? && !para_dom.empty? && !para_dom.to_html.strip.empty? && para_dom.to_html.strip.length>250
    
    {:dom => content_dom, :img_dom => img_dom }
  end

  def get_2_content_imgs(img_dom, article_link)
    img_url1 = img_dom.first.attributes['src'].value    
    img_url1 = article_link[0...article_link.index('/', 8)] +"/" + img_url1 unless img_url1.start_with?("http")
    img_url1 = "http://" + img_url1.gsub("..","")
                                   .gsub(/^http:\/\//,"")
                                   .gsub(/\/{2,}/,"/")

    img_url2 = img_dom[img_dom.size/2].attributes['src'].value    
    img_url2 = article_link[0...article_link.index('/', 8)] +"/" + img_url2 unless img_url2.start_with?("http")
    img_url2 = "http://" + img_url2.gsub("..","")
                                   .gsub(/^http:\/\//,"")
                                   .gsub(/\/{2,}/,"/")
    img_urls = img_url1
    img_urls = img_url1 + ";" + img_url2 unless img_url1 == img_url2

    img_urls
  end

  def img_keywords
    [
        '.pane-node-field-image-and-desc-coll img',
        'img.vlz'
    ]
  end

  def content_keywords
    [
        '.pane-node-body',
        'div.column-1 .body',
        '#C-Main-Article-QQ',
        '.blkContainerSblk',
        '.ep-content-main #endText',
        '#artical_real',
        '#cont_2_ab_p #sv',
        '.mainCont #contentText',
        '.main .con .text', 
        '#main .article .article-body',
        '.content-detail .content-body',
        '.main .bd', 
        '.fiber_tob_wzb',
        '.entry-content #articleBody',
        '#storysection .storybody', 
        '.td_padding_right .doc_annotation',
        '.article .ref-text',
        '#sectionleft4 #zw',
        '#OPODO-SEC-C',
        '.tt-article-text', 
        '#lc #Container #text',
        '.box .colborder .article',
        '.ArticleBlog .ArticleBlogText',
        '.entrycontent_single',
        '.sbn-entry-body-inner',
        '.sty_kicker',
        '#cuerpo',
        '#matter #matterc',
        '#bodyContent .col3',
        '#nwContent .contentText',
        '.sectionContent #articleText',
        '.column_list_inbox .article',
        '.wrappingContent',
        '#js-article-text',
        '.wordsnap .newstext',
        '#article #articleText',       
        '#content .entry-body',
        '.content #articleText',
        '.node .content',
        '.content .field-item',
        '#primary_content #story_content', 
        '.bodyback .bodytext',
        '#content .entry-content', 
        '#content .article .content',  
        '#content .article',  
        '#content #article',
        '.content .article',
        '#content #article',
        '.story-body .article',
        '#content .post-content', 
        '.storyfull-article', 
        '#main .newsText',
        '.Article #main_text',
        '#Article .content', 
        '.maincontent article',
        '#maincontent article',
        '.main .newsview',
        '.KonaBody', 
        '#contentArticle',
        '.contentArticle',  
        '#story_body',     
        '.story_body',        
        '#story-body', 
        '.story-body',       
        '#printcontent',
        '.printcontent',
        '#articleBody',
        '.articleBody',
        '.new_lei',
        '#cmArticleWell',
        '.news_con',
        '#news_con',
        '.selectable_content',
        '#selectable_content',
        '.newsBody',
        '#newsBody',
        '.news-body',
        '#news-body', 
        '.newsbody',
        '#newsbody',
        '.article .para',
        '#story_content',
        '.story_content',
        '#storyContent',
        '.storyContent',
        '#storycontent',
        '.storycontent',
        '.contentarea',
        '#contentarea',
        '.Newscontext',
        'td.bw', 
        '#col3_content',       
        '.article_body',
        '.article .body', 
        '#articles .pcont',
        '.ContentEditor',
        '.xn-content',       
        '#content .post', 
        '.content_body',
        '#content_body',       
        '#articleContent',
        '.articleContent', 
        '#mainContent',
        'mainContent',
        '#maincontent',
        '.maincontent',
        '#main-content',
        '.main-content',
        '#main_content',
        '.main_content',
        '#contentMain',
        '.contentMain',
        '#content_main',
        '.content_main',
        '#contentNews',
        '.contentNews',       
        '#content-main',
        '.content-main',
        '#articlecontent',
        '.articlecontent',
        '#contentcore',
        '.contentcore',
        '.blog .entry',
        '#p_content',
        '.content_p',
        '#artbody',
        '#artibody',
        'article .story',
        '#contentText',
        '#mediaarticlebody',        
        '.entry-content',       
        '.d-content',
        '.home #main-col', 
        '#mainBodyArea',
        '.mainBodyArea',
        '#main #zw',  
        '#page #main .text',
        '#wrapper .post',
        '#newsStory',
        '.newsStory',
        '#news_story',
        '.news_story',
        '#article main',
        '.article main',
        '#article_content',
        '.article_content',
        '#article-content',
        '.article-content',
        '#blog_article_content',
        '.blog_article_content',
        '#content_wrapper',
        '.content_wrapper',
        '#content-wrap',
        '.content-wrap',
        '#ContentBody',
        '#BodyLabel', 
        '#mediablogbody',
        '.inner_main_text',
        '#content-area .content',        
        '.newsCon', 
        '#bodytext',
        '.bodytext',
        '#newsContent', 
        '.newsContent',
        '#newscontent', 
        '.newscontent',
        '#news-content', 
        '.news-content',
        '#news_content',
        '.news_content',
        '#the_content',
        '.the_content',
        '#thecontent',
        '.thecontent',
        '#the-content',
        '.the-content',
        '#content-area',
        '.content-area',
        '#contentArea',
        '.contentArea',
        '#content_area',
        '.content_area',
        '#body-content',
        '.body-content',
        '#article-text',
        '.article-text',
        '#articleText',
        '.articleText',
        '#article_text',
        '.article_text',
        '#ArticleText',
        '.ArticleText', 
        '#textContent',
        '#content-wrap',
        '.content-wrap',
        '#content-wrapper',
        '.content-wrapper',
        '#contentSection',
        '.contentSection',
        '.ctx_content',
        '#news_c',
        '#featurecontent',
        '#Content',
        '.Content',
        '#CONTENT',
        '.CONTENT',
        '#contents',
        '.contents',
        '#Contents',
        '.Contents',
        '#content',
        '.content'    
        
    ]
  end

end
