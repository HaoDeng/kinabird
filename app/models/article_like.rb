class ArticleLike < ActiveRecord::Base
  belongs_to :article, :inverse_of => :article_likes
  belongs_to :user,    :inverse_of => :article_likes

	default_scope order('created_at desc')
	
end

