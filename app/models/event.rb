class Event < ActiveRecord::Base
	paginates_per 15
  max_paginates_per 100

  attr_accessible :event_date, 
  								:event_date_str,
  								:event_type, 
  								:event_url, 
  								:logo_url,
  								:city,
  								:location, 
  								:name, 
  								:provider
  								
	scope :image_12_events, where("logo_url is not null and event_date > ?", DateTime.now)
													.order('event_date').limit(50)
													.to_a
													.uniq { |event| event.name }[0...12]

	def self.type_events(type, city, year, month)
		sql = event_type_sql(type)
		sql += " and city = '#{city}'" 					 		 if city
		sql += " and year(event_date) = '#{year}'"   if year
		sql += " and month(event_date) = '#{month}'" if month

		Event.where("#{sql} and event_date > ?", DateTime.now)
				 .order('event_date')
	end

	def self.city_type_count(type, city)
		sql = "select city, count(1) as count from events where " 
		sql += event_type_sql(type)
		sql += "group by city order by count(1) desc"
		records_array = ActiveRecord::Base.connection.execute(sql)
		records_array.to_a if records_array
	end

	def self.city_month_type_count(type, city)
		sql = "select year(event_date), month(event_date), count(1) from events where city='#{city}' and "
		sql += event_type_sql(type)
		sql += "and event_date > CURDATE() "
		sql += "group by year(event_date), month(event_date)"
		records_array = ActiveRecord::Base.connection.execute(sql)
		records_array.to_a if records_array
	end

	private 
	def self.event_type_sql (type)
		if 'Conference' == type
			sql = "(event_type like '%Conference%' or event_type like '%Class%') "
		elsif 'Sport' == type
			sql = "(event_type like '%Sport%' or event_type like '%Outdoor%') "
		elsif 'Networking' == type
			sql = "(event_type like '%Network%' or event_type like '%Social%' or event_type like '%Business%') "
		else
			sql = "event_type like '%#{type}%' "
		end
	end
end
