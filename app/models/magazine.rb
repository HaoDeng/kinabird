class Magazine < ActiveRecord::Base
  attr_accessible :from_date, 
  								:super_market, 
  								:to_date, 
  								:magazine_type,
  								:logo_url,
  								:first_img_url,
  								:page_count

	has_many :magazine_pages


	# magazine hash, key: category
	def self.latest_magazine_hash
		magazines = Magazine.where("to_date > ?", DateTime.now).order("super_market")

		magazine_hash = Hash.new
		magazines.each do |m|
			if !magazine_hash.has_key?(m.magazine_type)
				magazine_hash[m.magazine_type] = [m]
			else
				magazine_hash[m.magazine_type] << m
			end 
		end

		magazine_hash
	end

end
