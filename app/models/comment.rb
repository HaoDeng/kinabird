class Comment < ActiveRecord::Base
  attr_accessible :content, 
  								:like

  belongs_to :user,    :inverse_of => :comments
  belongs_to :article, :inverse_of => :comments

  validates_presence_of :content
  
  default_scope order('created_at desc')
 
  
end
