class Article < ActiveRecord::Base
  paginates_per 28
  max_paginates_per 100

  attr_accessible :content,
                  :summary, 
  								:link, 
  								:source, 
  								:published_at, 
  								:title, 
  								:imgurl, 
  								:guid,
                  :votes,
                  :status

  has_many :comments
  has_many :article_likes

  validates_presence_of :title,
                        :link,
                        :summary

  default_scope order('published_at desc')       
  scope :latest_12_articles, where(:status => 0).reorder('created_at desc').limit(12)

  before_save :encode

  def self.lang(lang, provider)
    if lang
      provider.nil? || provider.empty? ? Article.where(:language =>lang, :status => 0) 
                                       : Article.where(:language =>lang, :status => 0, :source => provider) 
    else
      Article
    end
  end

  def like (user)
    mylike = ArticleLike.where(:user_id => user.id, 
                               :article_id => self.id).first

    if mylike.nil?
      self.votes +=1
      self.save
      
      like = self.article_likes.build
      like.user_id = user.id
      like.save
    end
    like
  end

  protected
  def encode
  	# double encode to remove unknown UTF-8 chars
    if self.content
      self.content.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
      self.content.encode!('UTF-8', 'UTF-16')
    end
		
    if self.title
		  self.title.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
  	  self.title.encode!('UTF-8', 'UTF-16') 
    end

    if self.summary
      self.summary.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
      self.summary.encode!('UTF-8', 'UTF-16') 
    end	
  end
end
