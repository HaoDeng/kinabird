class User < ActiveRecord::Base
  attr_accessible :email, 
  								:name, 
  								:picurl, 
  								:provider, 
  								:uid
                  :provider_url

  has_many :comments,      dependent: :destroy
  has_many :article_likes, dependent: :destroy

  validates_presence_of :name

  def update_profile(auth)
    user = User.set_user_info(self, auth)
    user.save
  end

  def self.create_with_omniauth(auth)
    create! do |user|
      set_user_info(user, auth)
    end
  end

  def get_activities(count)
    activities = Array.new
    activities << self.comments[0..count] << self.article_likes[0..count]

    # user activities in desc order
    activities.flatten!.sort! {|a,b| b.created_at <=> a.created_at }
    activities = activities[0...count]
  end

  private
  def self.set_user_info(user, auth)
    user.provider = auth['provider']
      user.uid = auth['uid']
      if auth['info']
          user.name = auth['info']['name'] || user.name
          user.email = auth['info']['email'] || user.email
          
          if "linkedin" == auth['provider']
            user.picurl=auth['extra']['raw_info']['pictureUrl'] ||""
            user.provider_url=auth['extra']['raw_info']['publicProfileUrl'] ||""
          elsif "facebook" == auth['provider']
            user.picurl=auth['info']['image']||""
            user.provider_url=auth['extra']['raw_info']['link']||""
          elsif "twitter" == auth['provider']
            user.picurl=auth['info']['image']||""
            user.provider_url=auth['info']['urls']['Twitter']||""
          elsif "tqq" == auth['provider']
            user.picurl=auth['info']['image']||""
            user.provider_url='http://' << auth['info']['urls']['Tqq']||""
          end
      end
      user
  end

end
