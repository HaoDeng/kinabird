class MagazinePage < ActiveRecord::Base
  paginates_per 12
  max_paginates_per 18

  attr_accessible :image_url, 
  								:magazine_id, 
  								:page_number

	belongs_to :magazine
end
