module EventsHelper

	def event_type_str(event_type)
		type='Music'
		if event_type.include?('Conference')
			type = 'Conference'
		elsif event_type.include?('Sport') || event_type.include?('Outdoor')
			type = 'Sport'
		elsif event_type.include?('Network') || event_type.include?('Business')
			type = 'Networking'
		elsif event_type.include?('Teater')
			type = 'Teater'
		elsif event_type.include?('Other')
			type = 'Other'
		end
		type
	end

	def selected_city_class(city)
		'selected-city' if params[:city] == city
	end

	def selected_year_month_class(year, month)
		'selected-year-month' if params[:year] == year && params[:month] == month
	end

end
