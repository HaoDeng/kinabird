# encoding: utf-8

module MagazinesHelper
	def active_item(page_number)
		# 12 items each page, the 1st one is active
		"active" if page_number%12 == 1
	end

	def get_from_to_date_str(magazine)
		magazine.from_date.strftime('%d/%m') << ' - ' << magazine.to_date.strftime('%d/%m')
	end

	def get_magazine_type(category)
	  case category
	  when "Dagligvarer"
	    "超市"
	  when "Autoudstyr"
	    "摩托汽配"
	  when "Byggemarkeder"
	    "DIY"
	  when "Grænsehandel"
	    "边境商店 - 超赞!"
	  when "Hvidevarer/elektronik"
	    "电器产品"
	  when "Isenkram"
	    "家居"
	  when "Legetøj/Hobby"
	    "玩具"
	  when "Møbler/indretning"
	    "家具装潢 "
	  when "Pc/elektronik"
	    "PC电子"
	  when "Stormagasiner"
	    "商场"
	  when "Bøger/kontorartikler"
	  	"办公用品"
	  else
	    "其他"
	  end
	end

end
