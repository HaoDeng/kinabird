module UserHelper
  def get_edit_submit_text
	  if user_signed_in? 
  		  "Submit" 
  	  else
  		  "Sign in"
  	  end
	end

	def logo_name
		if "linkedin" == @user.provider
			"in.png"		
		elsif "facebook" == @user.provider
			"fb.png"			
		elsif "twitter" == @user.provider
			"tw.png"	
		elsif "tqq" == @user.provider
			"tqq.png"					
		end
	end
end