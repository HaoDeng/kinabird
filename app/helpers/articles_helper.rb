module ArticlesHelper
	def liked? (article, user)
		ArticleLike.where(:user_id => user.id, 
                      :article_id => article.id).first
	end

	def selected_lang_class(lang)
		'selected-lang' if params[:lang] == lang
	end
end
