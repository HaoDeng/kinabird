
function add_comment(obj){
	area = $(obj).prevAll('textarea');
	value = area.val();
	if(value){
		area.val(value.substring(0,200));
		form = $(obj).closest('form');
		form.submit();
		form.hide();
	}
}

function like(article_id){
	$.ajax({
	  url: "article/" + article_id +"/like",
	  data: { id: article_id}
	}).done(function( msg ) {
	  // ignore
	});
}

function load_more_comments(article_id){
	$.ajax({
	  url: "article/" + article_id +"/load_more_comments",
	  data: { id: article_id}
	}).done(function( msg ) {
		// ignore
	});
}