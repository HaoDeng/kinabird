class UsersController < ApplicationController
  before_filter :authenticate_user!, :except => :show

  def edit
    @user = User.find(params[:id])
    can_modify?(@user)
  end
  
  def update
    @user = User.find(params[:id])
    can_modify?(@user)
    if @user.update_attributes(params[:user])
      redirect_to root_path
    else
      render :edit
    end
  end

  def show
    @user = User.find(params[:id])
    @activities = @user.get_activities(20)
  end

  def more_activities
    @user = User.find(params[:id])
    
    @activities = @user.get_activities(40)
    respond_to do |format|
        format.js
    end
  end

  private
  def can_modify? (user)
    if current_user.id != user.id
      redirect_to root_url, :alert => 'You cannot access to this page.'
    end
  end

end
