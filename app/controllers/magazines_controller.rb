class MagazinesController < ApplicationController

	def index
		@magazines_hash = Magazine.latest_magazine_hash
	end

	def show
		@magazine = Magazine.find(params[:id])
		@magazine_pages = @magazine.magazine_pages.page(params[:page])
	end
end
