class CommentsController < ApplicationController
	
	def create
		if current_user
	    @article = Article.find(params[:article_id])
	    @comment = @article.comments.build(params[:comment])
	    @comment.user_id = current_user.id
	    @comment.save
  	end
    
    respond_to do |format|
        format.js
    end
  end
end
