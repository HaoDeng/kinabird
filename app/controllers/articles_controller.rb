class ArticlesController < ApplicationController

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.lang(params[:lang], params[:provider]).page(params[:page])
    articles_in_4_col(@articles)
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    @article = Article.find(params[:id])
  end

  # GET /articles/new
  # GET /articles/new.json
  def new
    @article = Article.new

  end

  # GET /articles/1/edit
  def edit
    @article = Article.find(params[:id])
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(params[:article])

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render json: @article, status: :created, location: @article }
      else
        format.html { render action: "new" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /articles/1
  # PUT /articles/1.json
  def update
    @article = Article.find(params[:id])

    respond_to do |format|
      if @article.update_attributes(params[:article])
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    respond_to do |format|
      format.html { redirect_to articles_url }
      format.json { head :no_content }
    end
  end

  def like
    if current_user
      @article = Article.find(params[:id])
      @mylike = @article.like (current_user)
    end

    respond_to do |format|
      format.js
    end
  end

  def load_more_comments
    if current_user
      @article = Article.find(params[:id])
    end

    respond_to do |format|
      format.js
    end
  end


  protected
  def articles_in_4_col(articles)
    @articles_col1 = Array.new
    @articles_col2 = Array.new
    @articles_col3 = Array.new
    @articles_col4 = Array.new

    i = 1
    articles.each do |article|
      @articles_col1 << article if 1 == i
      @articles_col2 << article if 2 == i
      @articles_col3 << article if 3 == i
      @articles_col4 << article if 4 == i
      
      i = 0 if i>=4
      i+= 1
    end
  end
end
