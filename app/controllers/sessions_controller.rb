class SessionsController < ApplicationController
  def new
  end

  def create
    auth = request.env["omniauth.auth"]
    p auth
    user = User.where(:provider => auth['provider'], :uid => auth['uid'].to_s).first
    if user
      user.update_profile(auth)
    else
      user = User.create_with_omniauth(auth)
    end
    
    session[:user_id] = user.id
    if user.email.blank?
      redirect_to edit_user_path(user), :alert => "Please enter your email address."
    else
      redirect_to root_url
    end
  end

  def destroy
    reset_session
    redirect_to root_url
  end

  def failure
    redirect_to root_url, :alert => "Authentication error: #{params[:message].humanize}"
  end

end