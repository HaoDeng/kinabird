#encoding: utf-8
class HomeController < ApplicationController

	def index
		articles_in_4_col(Article.latest_12_articles)
		@events = Event.image_12_events

    # begin
    #   @tweets=Twitter.search("@哥本哈根", :count => 30).results
    # rescue Exception => e
    #   p e
    #   @tweets = []
    # end 

	end

	protected
  def articles_in_4_col(articles)
    @articles_col1 = Array.new
    @articles_col2 = Array.new
    @articles_col3 = Array.new
    @articles_col4 = Array.new

    i = 1
    articles.each do |article|
      @articles_col1 << article if 1 == i
      @articles_col2 << article if 2 == i
      @articles_col3 << article if 3 == i
      @articles_col4 << article if 4 == i
      
      i = 0 if i>=4
      i+= 1
    end
  end

end
