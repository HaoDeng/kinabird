class EventsController < ApplicationController

  def index
    params[:city] = "Copenhagen" if params[:city].nil?

  	@events = Event.type_events(params[:type], 
                                params[:city], 
                                params[:year], 
                                params[:month]).page(params[:page])
  	events_in_3_col(@events)

  	@city_type_count = Event.city_type_count(params[:type], params[:city])
    @city_month_type_count = Event.city_month_type_count(params[:type], params[:city])
  end


  protected
  def events_in_3_col(events)
    @events_col1 = Array.new
    @events_col2 = Array.new
    @events_col3 = Array.new

    i = 1
    events.each do |event|
      @events_col1 << event if 1 == i
      @events_col2 << event if 2 == i
      @events_col3 << event if 3 == i
      
      i = 0 if i>=3
      i+= 1
    end
  end
end
