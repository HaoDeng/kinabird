# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
set :environment, 'production'

every 15.minutes do
  rake "parse:get_feeds", :output => {:error => 'error.log', :standard => 'cron.log'}
end

every 12.hours do
  rake "parse:get_events", :output => {:error => 'error.log', :standard => 'cron_events.log'}
end

every 12.hours do
  rake "parse:get_events_2", :output => {:error => 'error.log', :standard => 'cron_events.log'}
end

every 12.hours do
  rake "parse:get_events_3", :output => {:error => 'error.log', :standard => 'cron_events.log'}
end

every 15.hours do
  rake "parse:get_magazines", :output => {:error => 'error.log', :standard => 'cron_magaines.log'}
end