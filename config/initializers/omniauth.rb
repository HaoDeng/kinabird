Rails.application.config.middleware.use OmniAuth::Builder do

  provider :linkedin, ENV['LINKEDIN_APP_ID'], ENV['LINKEDIN_SECRET'], :scope => 'r_fullprofile r_emailaddress r_network'

  provider :facebook, ENV['FACEBOOK_APP_ID'], ENV['FACEBOOK_SECRET']

  provider :twitter, ENV['TWITTER_APP_ID'], ENV['TWITTER_SECRET']

  provider :tqq, ENV['TQQ_APP_ID'], ENV['TQQ_SECRET']

  OmniAuth.config.on_failure = Proc.new { |env|
    OmniAuth::FailureEndpoint.new(env).redirect_to_failure
  }
end