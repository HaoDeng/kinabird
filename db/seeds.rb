# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

magazines = Magazine.create([{ from_date: Time.now , to_date: Time.now , super_market: 'Aldi', magazine_type: 'food'}])

MagazinePage.create(page_number: 1, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-0.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 2, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-1.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 3, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-2.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 4, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-3.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 5, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-4.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 6, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-5.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 7, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-6.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 8, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-7.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 9, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-8.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 10, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-9.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 11, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-10.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 12, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-11.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 13, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-12.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 14, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-13.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 15, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-14.jpg' , magazine_id: magazines.first.id)
MagazinePage.create(page_number: 16, image_url:'http://static.tilbudsugen.dk/common/img/1st-retail/2013/14/21246/aldi142013_zoom-15.jpg' , magazine_id: magazines.first.id)