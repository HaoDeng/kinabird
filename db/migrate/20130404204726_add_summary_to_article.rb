class AddSummaryToArticle < ActiveRecord::Migration
  def up
  	add_column :articles, :summary, :text
  	Article.connection.execute("update articles set summary=content")
  	Article.connection.execute("update articles set content=null")
  end

  def down
  	Article.connection.execute("update articles set content=summary")
  	Article.connection.execute("update articles set summary=null")
  	remove_column :articles, :summary
  end
end
