class ChangeNamesInArticle < ActiveRecord::Migration
  def up
  	rename_column :articles, :timestamp, :published_at
  	change_column :articles, :published_at, :datetime
  end

  def down
  end
end
