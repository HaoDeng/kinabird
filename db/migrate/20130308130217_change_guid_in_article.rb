class ChangeGuidInArticle < ActiveRecord::Migration
  def up
    change_column :articles, :guid, :text, :limit => 1500
  end

  def down
  end
end
