class CreateMagazines < ActiveRecord::Migration
  def change
    create_table :magazines do |t|
      t.string :super_market
      t.datetime :from_date
      t.datetime :to_date
      t.string :magazine_type
      t.text :logo_url

      t.timestamps
    end
  end
end
