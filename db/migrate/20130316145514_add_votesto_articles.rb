class AddVotestoArticles < ActiveRecord::Migration
  def up
  	add_column :articles, :votes, :integer, :default =>0
  end

  def down
  	remove_column :articles, :votes
  end
end
