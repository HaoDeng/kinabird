class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :title
      t.string :timestamp
      t.string :source
      t.text :link
      t.text :content

      t.timestamps
    end
  end
end
