class AddStatusToArticles < ActiveRecord::Migration
  def up
  	add_column :articles, :status, :integer, :default =>0
  end

  def down
  	remove_column :articles, :status
  end
end
