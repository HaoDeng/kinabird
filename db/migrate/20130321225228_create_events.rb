class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.text :name
      t.text :event_url
      t.text :logo_url
      t.string :event_type
      t.string :event_date_str
      t.datetime :event_date
      t.string :location
      t.string :city
      t.string :provider

      t.timestamps
    end
  end
end
