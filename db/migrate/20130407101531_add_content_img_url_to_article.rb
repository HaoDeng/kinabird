class AddContentImgUrlToArticle < ActiveRecord::Migration
  def up
  	add_column :articles, :content_img_url, :text
  end

  def down
  	remove_column :articles, :content_img_url
  end
end
