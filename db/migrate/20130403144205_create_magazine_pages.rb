class CreateMagazinePages < ActiveRecord::Migration
  def change
    create_table :magazine_pages do |t|
      t.text :image_url
      t.integer :magazine_id
      t.integer :page_number


      t.timestamps
    end
  end
end
