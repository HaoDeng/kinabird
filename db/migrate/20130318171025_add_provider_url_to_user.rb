class AddProviderUrlToUser < ActiveRecord::Migration
  def up
  	add_column :users, :provider_url, :text
  end

  def down
  	remove_column :users, :provider_url
  end
end
