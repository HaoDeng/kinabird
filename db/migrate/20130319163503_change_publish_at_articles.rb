class ChangePublishAtArticles < ActiveRecord::Migration
  def up
  	change_column :articles, :published_at, :datetime, :limit => nil
  end

  def down
  end
end
