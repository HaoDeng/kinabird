# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130407101531) do

  create_table "article_likes", :force => true do |t|
    t.integer  "article_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "articles", :force => true do |t|
    t.text     "title"
    t.datetime "published_at"
    t.string   "source"
    t.text     "link"
    t.text     "content"
    t.datetime "created_at",                     :null => false
    t.datetime "updated_at",                     :null => false
    t.string   "imgurl"
    t.text     "guid"
    t.string   "language"
    t.integer  "votes",           :default => 0
    t.integer  "status",          :default => 0
    t.text     "summary"
    t.text     "content_img_url"
  end

  create_table "comments", :force => true do |t|
    t.text     "content"
    t.integer  "like"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "article_id"
    t.integer  "user_id"
  end

  create_table "events", :force => true do |t|
    t.text     "name"
    t.text     "event_url"
    t.text     "logo_url"
    t.string   "event_type"
    t.string   "event_date_str"
    t.datetime "event_date"
    t.string   "location"
    t.string   "city"
    t.string   "provider"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  create_table "magazine_pages", :force => true do |t|
    t.text     "image_url"
    t.integer  "magazine_id"
    t.integer  "page_number"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "magazines", :force => true do |t|
    t.string   "super_market"
    t.datetime "from_date"
    t.datetime "to_date"
    t.string   "magazine_type"
    t.text     "logo_url"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "picurl"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.text     "provider_url"
  end

end
